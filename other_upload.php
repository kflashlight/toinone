<?php

$fileName = $_FILES['files']['name'];
$fileType = $_FILES['files']['type'];
$fileContent = file_get_contents($_FILES['files']['tmp_name']);
$dataUrl = 'data:' . $fileType . ';base64,' . base64_encode($fileContent);

$json = json_encode(array(
  'id' => '2',		// Should return ID from PHP script after image uploaded and inserted into DB
  'name' => $fileName,
  'type' => $fileType,
  'dataUrl' => $dataUrl
));
 
echo $json;
?>