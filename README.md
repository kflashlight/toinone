ToInOne
=======

2 in 1 drag and drop file uploader

This is still first version, and still under development, but I have no time to finish this. 
It uses a normal uploader for the major browsers like IE, Chrome, Opera (newest) and Safari for IOS. 
For Safari for Windows, it uses the second uploader.

Features:

1. 2 Uploaders
2. Can drag files and folders (chrome v. 21 or newer)
3. Progressbar
4. Normal button upload

and more....

Feel free to fix the code and make it better. I say thank you to all help I can get.
