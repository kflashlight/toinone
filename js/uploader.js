;(function (undefined) {

'use strict';

// Holds different variables for the uploader.
var up = this.Uploader = {
	// Holds the ID on uploaded image after saved to DB...:
	idHolder: document.getElementById('uploadvalues')
    // The list in the UI to which uploads will be added
	, fileList: document.getElementById('file-uploads')
    // The list in the UI to which we will upload files
	, DropZone: document.getElementById('DropZone')
    , progress: document.getElementById('progress-bar')
    , first_li: document.getElementById('first_li')		
	
	// The maximum number of concurrent active uploads
	, MAX_ACTIVE_UPLOADS: 5
	// A list for uploads that are waiting for a free slot to start running
	, queue: []
	// A list for the uploads that are actively uploading
	, activeUploads: []
	// Internal tracker for the number of uploads that have been added
	, safari: []
	// Internal tracker for the number of uploads that have been added
	, uploadCount: null
    // Check if browser is Chrome...:
	, isChrome:  /chrome/.test(navigator.userAgent.toLowerCase())
    , list: []
	// Keep the total filesize for each uploaded file for the second uploader
	, totalSize: null
	// Keep the total progress for each uploaded file for the second uploader	
	, totalProgress: null
	// Cached document or speed improvments
	, doc: document
	// Cached window for speed improvments
	, _window: window	
};
	// Init applocation

	up.Init = function () {
	
	// Listen for changes on the file field and automatically upload any
	// selected files
	up.doc.querySelector('input[type=file]').addEventListener('change', up.uploadFromFileField, !1);

	var _this = this;

	// Listen for any clicks on the file upload "remove" links
	_this.fileList.addEventListener('click', up.removeUpload, !1);
	
	// Handle the drag and drop for adding files directly to the upload list
	_this.DropZone.addEventListener('dragover', up.stopEvent, !1); // DOM event
	_this.DropZone.addEventListener('dragenter', up.stopEvent, !1); // IE event
	_this.DropZone.addEventListener('drop', up.handleDrop, !1);
	_this.DropZone.addEventListener('dragleave', up.handleDragLeave, !1);

	// TODO! Fix the blinking effect that occur on IE and Safari on IOS when you drag a file on and off the drop area
	
	};

	// Remove an upload from the list when it's "remove" link is clicked, or 'NonBrowserRemove' is clicked for bad browsers...:
	up.removeUpload = function (ev) {
	var li, activeUpload;
	
	// Make sure the event was triggered by clicking a remove link
	if (ev.target.classList.contains('remove')) {
		// Fetch the list item for which the remove button was clicked
		li = ev.target.parentNode.parentNode;
	
//			if (li) {
//				}

			// Remove the list item from the upload list in the UI
			up.fileList.removeChild(li);
			ev.preventDefault();
	}

	// Make sure the event was triggered by clicking a remove link
	if (ev.target.classList.contains('NonBrowserRemove')) {

		// Fetch the list item for which the remove button was clicked
		li = ev.target.parentNode.parentNode;

		if (li) {
			
			// Delete image from database and server...:

			alert(li.getAttribute('pindio_set_img')); // Alert the ID that is sent back from server with ajax, so we can see it's working
		
		// Remove the list item from the upload list in the UI
		up.fileList.removeChild(li);
	}
	ev.preventDefault();
	}
  };
	
	// Remove the class when the object is removed from the DropZone...:

	up.handleDragLeave = function (ev) {
		up.doc.getElementById('file-wrapper').style.display = 'block'
		up.doc.getElementById('file-wrapper').className = "";	
		up.doc.getElementById('DropZoneInfo').style.display = 'none'	
		up.DropZone.className = "";
	};

	// Read through the folders user upload, and upload the files inside them.
	
	up.traverseFileTree = function(item, path) {
	  path = path || "";
		item.isFile ? item.file(function (item) {
				item.type.toLowerCase().match(/image.*/) && up.uploadSafariFiles(item, "")				
			}) : item.isDirectory && item.createReader().readEntries(function (entries) {
				for (var e = 0, g = entries.length; e < g; e++) up.traverseFileTree(entries[e],
					path + item.name + "/")
			})
	};
  
  // Handle drop...:
  
  // KNOWN ERROR!!   It give double output when you upload in Chrome because  it first do a loop and upload, and then continue with
				  // the other upload that is outside is chrome{}. Put a false value here, make it to only upload one photo even if you try to upload multiple.
  
  up.handleDrop = function (ev) {
        up.stopEvent(ev);
		if (!ev.dataTransfer || typeof(ev.dataTransfer.files) === "undefined") return;
            up.doc.getElementById("file-wrapper").style.display = "block";
            up.doc.getElementById("file-wrapper").className = "";
            up.doc.getElementById("DropZoneInfo").style.display = "none";
            up.DropZone.className = "";
            if (up.SupportBlobChanger()) { // If browsers support blob. Safari on Windows don't support it
                for (var c = 0, d = ev.dataTransfer.files.length; c < d; c += 1) {
                    var e = ev.dataTransfer.files[c];
				    if (up.isChrome) { // Chrome v.21 and higher is the only browser who support folders, and file API.
                        var f = ev.dataTransfer.items[c].webkitGetAsEntry();
                        ev.dataTransfer.files[c].name.match(/\.zip/) ? up.unzip(file) : f.isFile ? ev.dataTransfer.files[c].type.toLowerCase().match(/image.*/) && up.UploadFile(ev.dataTransfer.files[c]) : f.isDirectory && up.traverseFileTree(f)
                    }
                }
                e.type.toLowerCase().match(/image.*/) && up.UploadFile(e)
            }
            up.processFiles(ev.dataTransfer.files);
            return !1
    };


	// Upload files from button...:
	
   up.uploadFromFileField = function (ev) {
	
	var _this = this;
	
	// If browser support blob...:	
	
	if (up.SupportBlobChanger()) {
		 var files = _this.files;

		for (var i=0, len = files.length; i< len; i+=1) {

			if( files[i].type.toLowerCase().match(/image.*/)) {

				up.UploadFile(files[i]);
			}
		}
		return -1; // Has to stop it here, else it will process a second upload and give double output.
	}

	// If not BLOB support, run the other uploader;
    up.processFiles(up.filterImages(_this.files));
        return !1
	};


 // upload next file

	up.uploadNext =
        function () {
            if (up.list.length) {
                var next = up.list.shift();
                if (20971520 <= next.size) return alert("Your file has to be less then 20 MB"), -1; // 20480kb - 20 MB
                up.uploadSafariFiles(next, status)
            } else up.fileList.className = ""
    };

	// Process  a bunch of files...:

	up.processFiles = function (files) {
        if (files && files.length && !up.list.length) {
            up.totalSize = 0;
            up.totalProgress = 0;

			// Add the selected files to the file queue
            for (var c, fileNames = {}, i = 0, len = files.length; i < len; i += 1) c = files[i], fileNames[c.name] || (fileNames[c.name] = !0, up.list.push(c), up.totalSize += c.size);
            up.uploadNext()
        }
    };
	
	
		
// Upload files with the Safari browser...:
up.uploadSafariFiles = function (file, status) {

        var xhr = up.NewXHR();
        xhr.upload.addEventListener("progress", up.SafariUploadProgress, !1);
        xhr.addEventListener("load", up.SafariUploadComplete, !1);
        xhr.addEventListener("error", up.SafariuploadFailed, !1);
        xhr.addEventListener("abort", up.SafariUploadCanceled, !1);
        xhr.open("POST", "other_upload.php");
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.setRequestHeader("Cache-Control", "no-cache");
		// Show the progressbar...:
        
		up.progress.style.display = "block";
        var fd = new FormData;
        fd.append("files", file);
        xhr.send(fd);

		// A quick fix is to reset the formdata object
        new FormData;
		// Process the next file in the queue...:
        up.uploadNext()

};
	// Progressbar for Safari upload...:

	up.SafariUploadProgress = function (evt) {
	
		if (evt.lengthComputable) {
			evt = Math.round(100 * evt.loaded / evt.total)
			var percent = up.doc.getElementById('percent');
		// Increase the progress bar length.
		up.doc.getElementById('uploaded').style.width = evt + '%';
		// Show the counter...:
		percent.textContent = evt + '%';
		}
	},

up.SafariUploadComplete = function(evt) {

  if (4 == this.readyState && 200 == this.status) {
	
	evt = JSON.parse(this.response);
	
	// Create a li element...:
	
	var li = up.doc.createElement('li')
	  , newValue = evt.id + ',' + up.idHolder.getAttribute('value');

	li.setAttribute('pindio_set_img', evt.id);

	
				// Save image id...:
				
				if(newValue!='undefined,') {
					up.idHolder.setAttribute('value', newValue)
			   }

	// Make some innerHtml data
	// Note! I use this solution and not append, because its faster and append is expensive.

	var	html = '<div class="NonBrowserRemove">X</div>';
		html += '<div class="preview">';
		html += '<img src="' + evt.dataUrl + '">';
		html += '</div>';

	// Append html to li element...:
	
	li.innerHTML = html;
	
	// Append li element to document...:

	up.fileList.appendChild(li);

	// Hide the progress indicator...:

	up.progress.style.display = 'none';
	
	}
	};

	up.SafariuploadFailed = function(evt) {
	alert("There was an error attempting to upload the file.")
	}
	
	// Show a popup message in the browser window if user manually refresh the web browser before upload is finished...:

	// FIX ME!! Extend the popup with two buttons - Continue and Cancel. If user click cancel, the upload continue as normal. 
	//          Else, cancel upload and remove the files from server

	up.SafariUploadCanceled = function(evt) {
		evt = up.doc.getElementById("upload_cancel");
		evt.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
		return -1;
	};

	//TODO !! Add function for extraction of zip folders and upload the files with the second uploader...:
	
	up.unzip = function(zip){

    	alert("Not implemented yet!");
	};
 

	// Maybe make this better?
	
	up.SupportBlobChanger = function () {

	 return up._window.URL ? !0 : up._window.webkitURL ? !0 : up._window.createObjectURL ? !0 : up._window.createBlobURL ? !0 : !1
	};


	up.UploadFile = function(file) {
	
	// any images to upload?
	
	if ( ! file.length) return; 
	
	// FIX ME!! Put this in it's own function and give it a pretty look
	// 		  Maybe with fallback to second uploader instead of this "Not supported!"

	if (up._window.URL){ var url = up._window.URL.createObjectURL(file);}
	else if (up._window.webkitURL){ var url = up._window.webkitURL.createObjectURL(file);}
	else if (up._window.createObjectURL){ var url = up._window.createObjectURL(file);}
	else if (up._window.createBlobURL){ var url = up._window.createBlobURL(file); }
	else { alert("Not supported!");	}


	var li = up.doc.createElement('li'), uploadId = (up.uploadCount += 1);

	var	html = '<div class="remove">x</div>';
		html += '<div class="preview">';
		html += '<img src="' + url + '">';
		html += '</div>';
		html += '<div class="progress"></div>';		

	/* NOTE!!   THIS PART NEED TO BE RE-WRITTEN AND FUNCION BELOW ADDED 
	
	var img = document.createElement('img');
					img.file = file;
					img.src = content;
					img.onload = function() {
						resize(this, configuration.maxHeight, configuration.maxWidth);

						// create HTML markup of thumbnail
						$queue.append(configuration.thumbnailTemplate.supplant({
							filename : this.file.name
						}));

	*/

	// Append html to li element...:
	
	li.innerHTML = html;

	// FIXME!! This upload ID should be the ID returned from server with ajax after images are uploaded to server.
	//	       When user click, the file is deleted, removed from queue and removed from database.
	//         See uploader 2 for inspiration
	
	// Create an object with all of the upload details and send it to queued  for uploading
	up.queueUpload({
			id: uploadId
			, file: file
			, listItem: li
		});

	up.fileList.appendChild(li);
	};


	// Queue a file for uploading. If there is space in the list of active
	// uploads then it is added directly to the active uploads and started as
	// there is no need to wait

	up.queueUpload = function (upload) {

	// Check available upload slots, and if any free, add to active uploads 
	// and start. If not, add the upload to the bottom of the queue stack	
	 up.activeUploads.length < up.MAX_ACTIVE_UPLOADS ? (up.activeUploads.push(upload), up.startUpload(upload)) : up.queue.unshift(upload)	
	};


   up.NewXHR = function () {
   try {
            return new XMLHttpRequest
        } catch (a) {
            for (var c = "MSXML2.XMLHTTP.6.0 MSXML2.XMLHTTP.5.0 MSXML2.XMLHTTP.4.0 MSXML2.XMLHTTP.3.0 MSXML2.XMLHTTP Microsoft.XMLHTTP".split(" "), e = 0; e < c.length && !request; e++) try {
                return new ActiveXObject(c[e])
            } catch (d) {}
        }}


		// Start upload with the first uploade and sets up all of the associated event handlers
		
		up.startUpload = function (upload) {
		
		var xhr = up.NewXHR();
		// Add a reference to the Ajax request on the upload object
		upload._xhr = xhr;
		// And likewise add a reference to the upload object on the Ajax request
		// and it's associated request upload so that it is easy to update the UI
		// state in the event handlers
		xhr._file_upload = xhr.upload._file_upload = upload;
		// Open the request and set the event listeners
		xhr.open('POST', 'upload.php', true);
		xhr.setRequestHeader('X-FILE-NAME', upload.file.name);
		xhr.onload = up.uploadComplete;
		xhr.onerror = up.uploadError;
		xhr.upload.onprogress = up.updateProgressIndicator;
		
		// Send the File object as the Ajax request body
		xhr.send(upload.file);
		
		};

		// Event handler that runs when the file upload Ajax request has completed
		
		up.uploadComplete = function () {

			var li, nextUpload, newValue = this.response + ',' + up.idHolder.getAttribute('value');
	
		// Request did not return with a 200
		
		if (this.status >= 400) {
		up.uploadError.call(this);
		return;
		}
	
		// Image ID is returned from server with response and saved in a "hidden field" for further threatment.
				
		if(newValue!='undefined,')  up.idHolder.setAttribute('value', newValue)


		// Ensure that the file upload object set on the Ajax request referenced
		// using the this keyword exists
		if (this._file_upload) {
		// Get the UI list item so that it can be updated
		li = this._file_upload.listItem;
		// Ensure the progress bar is set to complete state
		li.querySelector('.progress').style.width = '100%';
		// Add complete class to the list item
		li.classList.add('complete');
		
		// Update the application state now that this upload has finished and
		// set the next upload to start
		up.clearUpload(this);
		}
	};

	// Update the state of the UI if the upload fails
	up.uploadError = function (ev) {
	var li;
	
	// Ensure that the reference to the upload can be retrieved
	if (this._file_upload) {
	// Set the error state of the list item by adding the error class
	li = this._file_upload.listItem;
	li.classList.add('error');
	};

	// Clean up the application state
	up.clearUpload(this);
	};

	// Update the uploads progress indicator with the percentage of the upload
	// that has successfully uploaded
	up.updateProgressIndicator = function (ev) {
	
		var li;
	// Ensure the progress can be obtained from the event object
			ev.lengthComputable && this._file_upload && (li = this._file_upload.listItem, ev = 100 * (ev.loaded / ev.total), li.querySelector(".progress").style.width =
				ev + "%")
	};

	// Set the next upload to start, if it exists
	up.nextUpload = function () {
		
		  var next = up.queue.pop();
		// If there is a queued upload
		// Add it to the list of active uploads and start it
		next && (up.activeUploads.push(next), up.startUpload(next))
	};

	// Remove and return the upload with the matching id from the list of
	// active uploads. If it does not exist undefined is returned
	up.removeActiveUploadById = function (uploadId) {
	var activeUpload;
	
	return activeUpload;
	};

	
	// Filter the file list and return an array of only the images

	up.filterImages = function (fileList) {

		// Set up a Regex to check the file types agaist
		var acceptedType = /^image\//,
			filteredArray = [];

		for (var i = 0, len = fileList.length; i < len; i++) {

			if(acceptedType.test(fileList[i].type) && ! fileList[i].name.toLowerCase().match( '/<script>/') ) filteredArray.push(fileList[i]);
		}
	// Return array 
	return filteredArray;
	};

	// Remove the active upload and kill any references
	up.clearUpload = function (xhr) {
	var upload = xhr._file_upload;
	
	// Ensure the reference to the file upload can be retrieved
	if (upload) {
	// Remove the active upload
	up.activeUploads.splice(up.activeUploads.indexOf(upload), 1);
	// Delete the reference on the upload to the Ajax request
	delete upload.xhr;
	};

	// Delete the reference to the file upload object on the Ajax request
	// and it's associated upload
	delete xhr._file_upload;
	delete xhr.upload._file_upload;
	
	// Start the next upload
	up.nextUpload();
	};
	
	// Basic rezise function for uploaded files.
	// TODO!! Use Canvas
	
	up.Rezise = function (img, maxHeight, maxWidth) {
    var height = img.naturalHeight,
        width = img.naturalWidth,
        newWidth = null,
        newHeight = null;
    height / maxHeight >= width / maxWidth ? (newHeight = height > maxHeight ? maxHeight : height, newWidth = parseInt(newHeight * width / height)) : (newWidth = width > maxWidth ? maxWidth : width, newHeight = parseInt(newWidth * height / width));
    img.height = newHeight;
    img.width = newWidth
};

	// Block browser default drag over and make some css animation

	// FIXME! Make this a liter cleaner, and fix the blinking that occur when drag over with mouse

	up.stopEvent = function (ev) {
		
		up.doc.getElementById('file-wrapper').style.display = 'none'
		up.doc.getElementById('file-wrapper').className = "fw";
		up.doc.getElementById('DropZoneInfo').style.display = 'block'	
		up.DropZone.className = "DropZonee";
		if (ev.dataTransfer || typeof(ev.dataTransfer.files) !== "undefined") {
				ev.stopPropagation();
				ev.preventDefault();
			}
	};
	

up.Init();

}).call(this);